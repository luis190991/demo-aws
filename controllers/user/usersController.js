const express = require('express');
const async = require('async');
const bcrypt = require('bcrypt');
const User = require('../../models/user');
const jwt=require('jsonwebtoken');



function list(req, res, next){


  // get the decoded payload and header
  var decoded = jwt.decode(req.token, {complete: true});
  console.log(req.user);

  let users = [];
  res.json(users);
}

function index(req, res, next){
  let id = req.params.id;
  console.log(`El id de usuario es ${id}`);
  let user = {};
  res.json(user);
}

function create(req, res, next){
  const name = req.body.name;
  const password = req.body.password;
  const lastName = req.body.lastName;
  const email = req.body.email;

  let user = new User();
  user.name = name;
  user.lastName = lastName;
  user.email = email;

  async.parallel({
    salt:(callback) =>{
      bcrypt.genSalt(10, callback);
    }
  }, (err, results) =>{
    bcrypt.hash(password, results.salt, (err, hash) => {
      user.password = hash;
      user.salt = results.salt;
      user.save()
      .then(obj => res.json({
            error: false,
            message: res.__('user.save'),
            objs: obj
          }))
      .catch(err => rescd.json({
            error: true,
            message: 'usuario no Guardado',
            objs: err
          }));
    });
  });
}

function update(req, res, next){
  res.render('index', {title:'se actualizo un elemento'});
}

function destroy(req, res, next){
  res.render('index', {title:'elimino un elemento'});
}

module.exports = {
  index, list, create, update, destroy
}
