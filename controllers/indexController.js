const express = require('express');
const jwt=require('jsonwebtoken');
const config = require('config');
const async = require('async');
const bcrypt = require('bcrypt');

const User = require('../models/user');

const jwtKey = config.get('secret.key');

var usuario= {
 name:"luis",
 password:"secret"
}

module.exports = {
  login: (req,res) => {
    async.parallel({
      user: (callback) =>
       User.findOne({email:req.body.email})
       .select('password salt')
       .exec(callback)
    }, (err, results) =>{
      const user = results.user;
      if (user) {
        bcrypt.hash(req.body.password, user.salt, (err, hash) => {
           if (hash === user.password) {
              const token = jwt.sign(user.id, jwtKey);
              res.send(token);
            }else {
             res.status(401).json({
               error: true,
               message: res.__('user.err.pass'),
             });
            }
        });
      }else{
        res.status(401).json({
          error: true,
          message: res.__('user.err.pass'),
        });
      }
    });
 }
}
