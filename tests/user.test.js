const request = require('supertest')
const app = require('../app');

var token = null;

beforeAll(async () => {
  const res = await request(app)
    .post('/login')
    .send({
      email: 'luis@uach.mx',
      password: '1234567',
    });
    console.log("----->" + res);
    token = res.text;

});

describe('GET Endpoints', () => {
  it('should get list of users.', async () => {


    /*const res = await request(app)
      .post('/login')
      .send({
        email: 'luis@uach.mx',
        password: '1234567',
      });*/
    //  let token = "eyJhbGciOiJIUzI1NiJ9.NWRiZmMxMWUxNWRkNDYwNjdlZGY0ZDBj.AIV1e4zINBAYJyEfripWc_uwZ1zzc4Q3ZO8PViMr_to"


    console.log("----->" + token);
    const res = await request(app)
      .get('/users')
      .set( 'Authorization', 'Bearer ' + token)
      .send({});

    expect(res.statusCode).toEqual(200);

  });
});
